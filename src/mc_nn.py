import numpy as np
from Cluster import Cluster


class MC_NN(object):
    def __init__(self, n_features, errors_threshold):
        self._training_X = object
        self._training_y = object
        self._labels = 0
        self._n_labels = 0
        self._n_features = n_features
        self._clusters = []
        self._errors_threshold = errors_threshold
        self._timestamp_counter = 0
        self._time_threshold = 50.0
        self._forgetting_mechanism_status = 1

    @property
    def timestamp(self):
        return self.triangular(self._timestamp_counter)

    def fit(self, X, y):
        start_instances = int(y.size * 0.4)
        self._training_X = X
        self._training_y = y
        [self._labels, self._n_labels] = self.quantity_of_classes(y)
        print(self._labels)
        print(self._n_labels)

        self.start_clusters(X[:start_instances], y[:start_instances])

        for instance, label in zip(X[start_instances:], y[start_instances:]):
            self.training_predict(instance, label)

    def start_clusters(self, instances, instancesY):
        for label in self._labels:
            print("Criou")
            cluster = Cluster(label, self._n_features)
            self._clusters.append(cluster)

        for i, instanceY in enumerate(instancesY):
            for cluster in self._clusters:
                if instanceY == cluster.label:
                    cluster.append(instances[i], self._timestamp_counter)
                    if cluster.n == 1:
                        cluster.triangular_start_timestamp = self.triangular(self._timestamp_counter)
                    break

        for cluster in self._clusters:
            cluster.refresh_centroid()

    def predict(self, newInstance):
        distance = 0
        nn = np.zeros(2)
        for i, cluster in enumerate(self._clusters):
            distance = 0
            for j in range(self._n_features):
                distance += (cluster.centroid[j] - newInstance[j]) ** 2
            distance = distance ** (1 / 2)

            if i == 0:
                nn[0] = i
                nn[1] = distance
            else:
                if nn[0] > distance:
                    nn[0] = i
                    nn[1] = distance

        return self._clusters[int(nn[0])]._label

    def training_predict(self, newInstance, newInstanceLabel):
        self._timestamp_counter += 1
        distance = 0
        nn = np.array([-1, -1])
        sln = np.array([-1, -1])

        for i, cluster in enumerate(self._clusters):
            distance = 0
            for j in range(self._n_features):
                distance += (cluster.centroid[j] - newInstance[j]) ** 2
            distance = distance ** (1/2)
            if cluster.label == newInstanceLabel:
                if sln[0] == -1:
                    sln[0] = i
                    sln[1] = distance
                elif distance < sln[1]:
                    sln[0] = i
                    sln[1] = distance

            if nn[0] == -1:
                nn[0] = i
                nn[1] = distance
            else:
                if distance < nn[0]:
                    nn[0] = i
                    nn[1] = distance

        if self._clusters[int(nn[0])]._label == newInstanceLabel:
            self.correct_prediction(newInstance, int(nn[0]))
        else:
            self.wrong_prediction(newInstance, int(nn[0]), int(sln[0]))

        if self._forgetting_mechanism_status == 1:
            timestamp = self.timestamp
            for i, cluster in enumerate(self._clusters):
                if cluster.participation(timestamp) < self._time_threshold:
                    print("Deletou ")
                    self._clusters.pop(i)

    def correct_prediction(self, newInstance, clusterId):
        self._clusters[clusterId].error_decrement()

        self._clusters[clusterId].append(newInstance, self._timestamp_counter)
        self._clusters[clusterId].refresh_centroid()

    def wrong_prediction(self, newInstance, wrong_cluster_id, correct_cluster_id):
        self._clusters[wrong_cluster_id].error_increment()
        self._clusters[correct_cluster_id].error_increment()

        if (self._clusters[wrong_cluster_id].errors == self._errors_threshold) and (len(self._clusters[wrong_cluster_id].instances.shape) > 1):
            self.cluster_split(wrong_cluster_id)

        self._clusters[correct_cluster_id].append(newInstance, self._timestamp_counter)
        self._clusters[correct_cluster_id].refresh_centroid()

        if (self._clusters[correct_cluster_id].errors == self._errors_threshold) and (len(self._clusters[correct_cluster_id].instances.shape) > 1):
            self.cluster_split(correct_cluster_id)

    def cluster_split(self, clusterId):
        highest_variance = self._clusters[clusterId].highest_variance()

        negative_direction_vector = self._clusters[clusterId].clone_cfx1()
        positive_direction_vector = self._clusters[clusterId].clone_cfx1()

        for n, n2 in zip(negative_direction_vector, positive_direction_vector):
            n -= highest_variance[1]
            n2 += highest_variance[1]

        p = len(self._clusters)

        self._clusters.append(self._clusters[clusterId].clone_cluster()) #Clone 1
        self._clusters.append(self._clusters[clusterId].clone_cluster()) #Clone 2

        self._clusters[p - 2].append(negative_direction_vector, self._timestamp_counter)
        self._clusters[p - 1].append(positive_direction_vector, self._timestamp_counter)

        self._clusters[p - 2].refresh_centroid()
        self._clusters[p - 1].refresh_centroid()

        self._clusters.pop(clusterId)

    @staticmethod
    def triangular(x):
        return (x ** 2 + x) / 2

    @staticmethod
    def quantity_of_classes(y):
        classes = []
        isEqual = 0

        for label in y:
            isEqual = 0
            for element in classes:
                if label == element:
                    isEqual = 1
                    break
            if isEqual == 0:
                classes.append(label[0])

        return classes, len(classes)


