import numpy as np


class Cluster(object):
    def __init__(self, label, n_features):
        self._cfx1 = np.zeros((1, n_features))[0]
        self._cfx2 = np.zeros((1, n_features))[0]
        self._cft1 = 0
        self._centroid = np.zeros((1, n_features))[0]
        self._n = 0
        self._label = label
        self._errors = 0
        self._instances = np.array([])
        self._n_features = n_features
        self._triangular_start_timestamp = 0

    def set_instances(self, instances):
        self._instances = instances

    def set_n(self, n):
        self._n = n

    def set_cfx1(self, cfx1):
        self._cfx1 = cfx1

    def set_cfx2(self, cfx2):
        self._cfx2 = cfx2

    def set_cft1(self, cft1):
        self._cft1 = cft1

    def error_increment(self):
        self._errors += 1

    def error_decrement(self):
        if self._errors > 0:
            self._errors -= 1

    @property
    def label(self):
        return self._label

    @property
    def n(self):
        return self._n

    @property
    def centroid(self):
        return self._centroid

    @property
    def errors(self):
        return self._errors

    @property
    def instances(self):
        return self._instances

    def append(self, instance, timeStamp):
        for j in range(self._n_features):
            self._cfx1[j] += instance[j]
            self._cfx2[j] += (instance[j]) ** 2
        self._cft1 += timeStamp

        if self._n == 0:
            self._instances = np.hstack((instance, [timeStamp]))
        else:
            self._instances = np.vstack((self._instances, np.hstack((instance, [timeStamp]))))
        self._n += 1

    def refresh_centroid(self):
        if self._n != 0:
            for i in range(self._n_features):
                self._centroid[i] = self._cfx1[i] / self._n

    def participation(self, timestamp_MC_NN):
        if timestamp_MC_NN == self._triangular_start_timestamp:
            return 100
        return self._cft1 * 100 / (timestamp_MC_NN - self._triangular_start_timestamp)

    def clone_cfx1(self):
        return np.copy(self._cfx1)

    def clone_cluster(self):
        new_cluster = Cluster(self._label, self._n_features)
        new_cluster.set_instances(np.copy(self._instances))
        new_cluster.set_n(self._n)
        new_cluster.set_cfx1(np.copy(self._cfx1))
        new_cluster.set_cfx2(np.copy(self._cfx2))
        new_cluster.set_cft1(np.copy(self._cft1))
        return new_cluster

    def highest_variance(self):
        variances = np.zeros(self._n_features)
        for i in range(self._n_features):
            variances[i] = ((self._cfx2[i] / self._n) - ((self._cfx1[i] / self._n) ** 2)) ** (1 / 2)

        highest_variance = np.zeros(self._n_features)
        highest_variance[0] = 0
        highest_variance[1] = variances[0]

        for i, variance in enumerate(variances[1:]):
            if highest_variance[0] < variance:
                highest_variance[0] = i + 1
                highest_variance[1] = variance

        return highest_variance

