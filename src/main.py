from plot import plot
import numpy as np
import mc_nn
import Cluster

nFeatures = 2
threshold = 101000

datasetName = "../2CHT.txt"
dataset = np.loadtxt(fname=datasetName, delimiter=',')

dataset_X = dataset[:, :nFeatures]
dataset_y = dataset[:, dataset[0].size - 1:dataset[0].size]

dataset_X = dataset_X[:15000, :]
dataset_y = dataset_y[:15000]

n_windows = int(dataset_y.size / 200)
instances_per_window = int(dataset_y.size / n_windows)
percent = 0.4
errors = np.zeros(n_windows)
hits = np.zeros(n_windows)
hit_rate = np.zeros(n_windows)

for i in range(n_windows):
    classifier = mc_nn.MC_NN(nFeatures, threshold)
    classifier._forgetting_mechanism_status = 0
    size = i * instances_per_window
    size_2 = size + instances_per_window
    window_X = dataset_X[size:size_2, :]
    window_y = dataset_y[size:size_2]

    print(window_y)

    size = window_y.size
    size = int(size * percent)

    training_X = window_X[:size, :]
    training_y = window_y[:size]

    test_X = window_X[size:, :]
    test_y = window_y[size:]
    classifier.fit(training_X, training_y)

    for instance, instance_label in zip(test_X, test_y):
        if classifier.predict(instance) == instance_label:
            hits[i] += 1
        else:
            errors[i] += 1

for i in range(n_windows):
    hit_rate[i] = hits[i] / (hits[i] + errors[i])

print(hit_rate)

plot(np.array(range(n_windows + 1)[1:]) * instances_per_window, hit_rate, n_windows, instances_per_window)