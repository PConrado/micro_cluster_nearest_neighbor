import matplotlib.pyplot as plt

def plot(x, y, windows, size):
    plt.plot(x, y)
    plt.axis([size, windows * size , 0, 1])
    plt.show()